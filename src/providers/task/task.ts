import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';

/*
  Generated class for the TaskProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaskProvider {

  task = [];

  constructor(public afDB: AngularFireDatabase) {
    console.log('Hello TaskProvider Provider');
  }

  public getTask(){
    //return this.task;
    return this.afDB.list('gestorTareas/tareas/')
  }

  public getTaskById(id){
    return this.afDB.object('gestorTareas/tareas/'+id);
  }

  public createTask(task){
    //this.task.push(task);
    this.afDB.database.ref('gestorTareas/tareas/'+task.id).set(task);
  }

  public editTask(task){
    this.afDB.database.ref('gestorTareas/tareas/'+task.id).set(task);
  }

  public removeTask(task){
    this.afDB.database.ref('gestorTareas/tareas/'+task.id).remove();
  }

}
