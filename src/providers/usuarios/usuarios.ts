import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database/database';

/*
  Generated class for the UsuariosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuariosProvider {

  usuarios = [];
  user = [];

  constructor(public afDB: AngularFireDatabase) {
    console.log('Hello UsuariosProvider Provider');
  }

  public getUsers(){
    return this.afDB.list('gestorTareas/usuarios/')
  }

  public createUser(user){
    this.afDB.database.ref('gestorTareas/usuarios/'+user.id).set(user);
  }

}
