import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';


import { TaskProvider } from '../../providers/task/task';
import { UsuariosProvider } from '../../providers/usuarios/usuarios';

import * as firebase from 'firebase/app'

/**
 * Generated class for the AddTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {

  loading: Loading;
  task = {id: null, title: null, description: null, date: null, asigned: null, admin: null, state: null, user:null, fin: null};
  usuarios = [];
  user;
  currentUser;
  userNick;

  id:null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public taskProvider: TaskProvider,
    public usuariosProvider: UsuariosProvider,
    private loadingCtrl: LoadingController) {

  }

  ngOnInit() {
    this.usuariosProvider.getUsers().valueChanges()
      .subscribe( usuarios=> {
        this.usuarios = usuarios;
        console.log(this.usuarios)
        this.getAdmin(usuarios);
    });
  }

  getAdmin(usuarios){
    var user = firebase.auth().currentUser;
    console.log(user.email);
    for (let i of usuarios) {
      if(i.email == user.email){
        console.log(i); // "4", "5", "6"
        this.currentUser = i.id;
      }
    }
  }

  getUserNick(idUser): string{
    for (let i of this.usuarios) {
      if(idUser == i.id){
        return i.user;
      }
    }
  }

  saveTask(){
    this.showLoading();
    console.log(this.task.date);
    console.log(new Date(this.task.date));
    this.task.id = Date.now();
    this.task.user = this.getUserNick(this.user);
    this.task.asigned = this.user;
    this.task.admin = this.currentUser;
    this.task.state = 1;
    this.task.fin = 0;
    this.taskProvider.createTask(this.task);
    this.loading.dismiss();
    this.navCtrl.pop();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

}
