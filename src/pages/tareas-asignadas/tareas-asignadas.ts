import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { TaskProvider } from '../../providers/task/task';
import { UsuariosProvider } from '../../providers/usuarios/usuarios';

import * as firebase from 'firebase/app'

import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../../pages/login/login';

import { AddTaskPage } from '../../pages/add-task/add-task';
import { EditTaskPage } from '../../pages/edit-task/edit-task';

/**
 * Generated class for the TareasAsignadasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tareas-asignadas',
  templateUrl: 'tareas-asignadas.html',
})
export class TareasAsignadasPage {

  task = [];
  myTask = [];
  usuarios = [];
  currentUser;

  constructor(public navCtrl: NavController,
    public taskProvider: TaskProvider,
    public auth : AuthProvider,
    public usuariosProvider: UsuariosProvider) {
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.usuariosProvider.getUsers().valueChanges()
      .subscribe( usuarios=> {

        this.usuarios = [];
        this.usuarios = usuarios;
        this.getAdmin(this.usuarios);
    });
  }

  getAdmin(usuarios){
    var user = firebase.auth().currentUser;
    for (let i of usuarios) {
      if(i.email == user.email){
        this.currentUser = i.id;
      }
    }
    console.log(this.currentUser)
    this.getMyTask(this.currentUser);
  }

  getMyTask(currentUser){
    this.taskProvider.getTask().valueChanges()
      .subscribe( task=> {
        this.task = [];
        this.task = task;
        this.myTask = [];
        for(let i of this.task){
          if(i.admin == currentUser){
            this.myTask.push(i);
          }
        }
        console.log(this.myTask)
    });
  }
  

  cerrarSesion(){
    this.auth.logout();
    this.navCtrl.push(LoginPage);
  }

  eliminarTarea(item){
    console.log(item);
    this.taskProvider.removeTask(item);
  }

  goToAddTask(){
    this.navCtrl.push(AddTaskPage);
  }

  goToEditTask(id){
    this.navCtrl.push(EditTaskPage, {id:id});
    console.log(id);
  }

}
