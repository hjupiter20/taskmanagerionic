import { Component } from '@angular/core';

import { MisTareasPage } from '../mis-tareas/mis-tareas';
import { TareasAsignadasPage } from '../tareas-asignadas/tareas-asignadas';

/**
 * Generated class for the TabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tareasAsignadasRoot = TareasAsignadasPage;
  misTareasRoot = MisTareasPage;


  constructor() {}

}
