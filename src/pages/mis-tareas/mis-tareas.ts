import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform } from 'ionic-angular';

import { TaskProvider } from '../../providers/task/task';
import { UsuariosProvider } from '../../providers/usuarios/usuarios';

import * as firebase from 'firebase/app'

import { LocalNotifications } from '@ionic-native/local-notifications';

import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../../pages/login/login';

/**
 * Generated class for the MisTareasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mis-tareas',
  templateUrl: 'mis-tareas.html',
})
export class MisTareasPage {

  task = [];
  myTask = [];
  usuarios = [];
  currentUser;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public taskProvider: TaskProvider,
    public usuariosProvider: UsuariosProvider,
    public auth : AuthProvider,
    private localNotifications: LocalNotifications,
    public alertCtrl: AlertController) {

  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.usuariosProvider.getUsers().valueChanges()
      .subscribe( usuarios=> {
        this.usuarios = [];
        this.usuarios = usuarios;
        this.getAdmin(this.usuarios);
    });
  }

  getAdmin(usuarios){
    var user = firebase.auth().currentUser;
    for (let i of usuarios) {
      if(i.email == user.email){
        this.currentUser = i.id;
      }
    }
    console.log(this.currentUser)
    this.getMyTask(this.currentUser);
  }

  getMyTask(currentUser){
    this.taskProvider.getTask().valueChanges()
      .subscribe( task=> {
        this.task = [];
        this.task = task;
        this.myTask = [];
        for(let i of this.task){
          if(i.asigned == currentUser){
            this.myTask.push(i);
          }
        }
        console.log("++++++++++++")
        console.log(this.myTask)
        console.log("++++++++++++")
        this.agendarTareas(this.myTask);
    });
  }

  agendarTareas(task){
    console.log("-----------------")
    for(var i= 0;i<task.length;i++){
      if(task[i]["state"]==1){
        this.localNotifications.schedule({
          id: task[i]["id"],
          title: task[i]["title"],
          text: task[i]["description"],
          data: { mydata: 'My hidden message this is' },
          at: new Date(new Date(task[i]["date"]).getTime() + (18000000) - (300000))
        });
        task[i]["state"] = 0;
        this.taskProvider.createTask(task[i]);
        console.log(new Date(new Date(task[i]["date"]).getTime() + (18 * 1000000) - (5*10000)))
      }
      console.log(new Date(new Date(task[i]["date"]).getTime() + (18000000) - (300000)))
      console.log("-----------------")
    }
  }

  cerrarSesion(){
    this.auth.logout();
    this.navCtrl.push(LoginPage);
  }

  acabarTarea(item){
    console.log(item);
    item.fin = 1;
    this.taskProvider.editTask(item);
  }

}
