import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';


import { TaskProvider } from '../../providers/task/task';
import { UsuariosProvider } from '../../providers/usuarios/usuarios';

/**
 * Generated class for the EditTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-edit-task',
  templateUrl: 'edit-task.html',
})
export class EditTaskPage {

  loading: Loading;
  task = {};
  usuarios = [];
  user;
  currentUser;
  userNick;
  id:null;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public taskProvider: TaskProvider,
    public usuariosProvider: UsuariosProvider,
    private loadingCtrl: LoadingController) {
      this.id = navParams.get('id');
      this.taskProvider.getTaskById(this.id).valueChanges()
        .subscribe(task=>{
        this.task = task;
        console.log(this.task);

      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditTaskPage');
  }

  saveTask(){
    this.showLoading();
    this.taskProvider.editTask(this.task);
    this.loading.dismiss();
    this.navCtrl.pop();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }
}
