import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { RegistroPage } from '../../pages/registro/registro';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  splash = true;
  loading: Loading;
  user= { email : '', password : ''};

  constructor(public navCtrl: NavController, public navParams: NavParams ,public auth : AuthProvider,
    public alertCtrl : AlertController,
    private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);
  }

  goToRegistro(){
    this.navCtrl.push(RegistroPage);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  login(){
    this.showLoading();
    this.auth.loginUser(this.user.email,this.user.password ).then((user) => {
      this.loading.dismiss();
    }).catch(err=>{
      this.showError(err);
    })
  }

  showError(text) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

}
