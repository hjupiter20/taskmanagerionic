import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UsuariosProvider } from '../../providers/usuarios/usuarios';

/**
 * Generated class for the RegistroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  loading: Loading;

  user= { id:null, user:null, email : '', password : ''};

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth : AuthProvider, public alertCtrl : AlertController, public userProvider : UsuariosProvider, private loadingCtrl: LoadingController) {
  }

  signin(){
    this.showLoading();
    this.user.id = Date.now();
    this.auth.registerUser(this.user.email,this.user.password).then((user) => {
      console.log(this.user);
      this.userProvider.createUser(this.user);
      this.loading.dismiss();
    }).catch(err=>{
      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: err.message,
        buttons: ['Aceptar']
      });
      alert.present();
    })
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

}
