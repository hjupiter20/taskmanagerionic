import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { LoginPage } from '../pages/login/login';
import { AddTaskPage } from '../pages/add-task/add-task';
import { RegistroPage } from '../pages/registro/registro';
import { TabsPage } from '../pages/tabs/tabs';
import { MisTareasPage } from '../pages/mis-tareas/mis-tareas';
import { TareasAsignadasPage } from '../pages/tareas-asignadas/tareas-asignadas';
import { EditTaskPage } from '../pages/edit-task/edit-task';

import { AuthProvider } from '../providers/auth/auth';
import { TaskProvider } from '../providers/task/task';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { UsuariosProvider } from '../providers/usuarios/usuarios';
import { LocalNotifications } from '@ionic-native/local-notifications';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    AddTaskPage,
    RegistroPage,
    TabsPage,
    MisTareasPage,
    TareasAsignadasPage,
    EditTaskPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    AddTaskPage,
    RegistroPage,
    TabsPage,
    MisTareasPage,
    TareasAsignadasPage,
    EditTaskPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LocalNotifications,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    TaskProvider,
    UsuariosProvider
  ]
})
export class AppModule {}
